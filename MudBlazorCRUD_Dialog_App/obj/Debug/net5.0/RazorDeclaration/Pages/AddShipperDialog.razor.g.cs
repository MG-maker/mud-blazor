// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace MudBlazorCRUD_Dialog_App.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "H:\Проекты для резюме\MudBlazorCRUD_Dialog_App\MudBlazorCRUD_Dialog_App\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "H:\Проекты для резюме\MudBlazorCRUD_Dialog_App\MudBlazorCRUD_Dialog_App\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "H:\Проекты для резюме\MudBlazorCRUD_Dialog_App\MudBlazorCRUD_Dialog_App\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "H:\Проекты для резюме\MudBlazorCRUD_Dialog_App\MudBlazorCRUD_Dialog_App\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "H:\Проекты для резюме\MudBlazorCRUD_Dialog_App\MudBlazorCRUD_Dialog_App\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "H:\Проекты для резюме\MudBlazorCRUD_Dialog_App\MudBlazorCRUD_Dialog_App\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "H:\Проекты для резюме\MudBlazorCRUD_Dialog_App\MudBlazorCRUD_Dialog_App\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "H:\Проекты для резюме\MudBlazorCRUD_Dialog_App\MudBlazorCRUD_Dialog_App\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "H:\Проекты для резюме\MudBlazorCRUD_Dialog_App\MudBlazorCRUD_Dialog_App\_Imports.razor"
using MudBlazorCRUD_Dialog_App;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "H:\Проекты для резюме\MudBlazorCRUD_Dialog_App\MudBlazorCRUD_Dialog_App\_Imports.razor"
using MudBlazorCRUD_Dialog_App.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "H:\Проекты для резюме\MudBlazorCRUD_Dialog_App\MudBlazorCRUD_Dialog_App\_Imports.razor"
using MudBlazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "H:\Проекты для резюме\MudBlazorCRUD_Dialog_App\MudBlazorCRUD_Dialog_App\Pages\AddShipperDialog.razor"
using MudBlazorCRUD_Dialog_App.Data.Models;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/Shipper")]
    public partial class AddShipperDialog : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 135 "H:\Проекты для резюме\MudBlazorCRUD_Dialog_App\MudBlazorCRUD_Dialog_App\Pages\AddShipperDialog.razor"
           
        bool fixed_header = true;
        private string searchString = "";

        bool validation;
        bool vcomp;

        //select title Create or Edit
        bool select=false;

        Shipper shipper = new Shipper();
        private List<Shipper> shippers = new List<Shipper>();
        private HashSet<Shipper> selectedItems = new HashSet<Shipper>();

        private List<Company> companies = new List<Company>();

        private HashSet<int> options { get; set; } = new HashSet<int>() { };

        protected override void OnInitialized()
        {
            GetShippers();
            GetCompanies();
            vcomp = false;
        }

        private void EqualList()
        {
            foreach (var option in options)
            {
                var comp = companyService.GetCompanyById(option);
                shipper.CompanyList.Append(comp.Name + ", ");
                shipper.CompanyShipperList.Add(option);
            }
            if (shipper.CompanyList.Length != 0)
                shipper.CompanyList.Remove(shipper.CompanyList.Length - 2, 2);
        }

        private void OnValidSubmit(EditContext context)
        {
            validation = true;
            StateHasChanged();

            if (shipper.Id == 0)
            {
                EqualList();
                shipperService.SaveShipper(shipper);
                snackBar.Add("Shipper Saved.", Severity.Success);
            }
            else
            {
                shipper.CompanyList.Clear();
                shipper.CompanyShipperList.Clear();
                EqualList();
                shipperService.UpdateShipper(shipper);
                snackBar.Add("Shipper Updated.", Severity.Normal);
            }
            vcomp = false;
            options.Clear();
        }

        private void OpenShipperDialog()
        {
            shipper = new Shipper();
            options.Clear();
            vcomp = true;
        }

        void Cancel()
        {
            options.Clear();
            vcomp = false;
        }

        private List<Company> GetCompanies()
        {
            companies = companyService.GetCompanies().ToList();
            return companies;
        }

        private List<Shipper> GetShippers()
        {
            shippers = shipperService.GetShippers().ToList();
            return shippers;
        }

        private bool SearchShipper(Shipper shipper)
        {
            if (string.IsNullOrWhiteSpace(searchString)) return true;
            if (shipper.Name.Contains(searchString, StringComparison.OrdinalIgnoreCase)
                || shipper.Phone.Contains(searchString, StringComparison.OrdinalIgnoreCase)
                 || shipper.Address.Contains(searchString, StringComparison.OrdinalIgnoreCase)
                )
            {
                return true;
            }
            return false;
        }


        private void Edit(int id)
        {
            select = true;
            shipper = shippers.FirstOrDefault(s => s.Id == id);
            foreach (var opt in shipper.CompanyShipperList)
            {
                options.Add(opt);
            }
            vcomp = true;
        }
        private void Delete(int id)
        {
            shipperService.DeleteShipper(id);
            snackBar.Add("Shipper Deleted.", Severity.Error);
        }

        private void DeleteAll()
        {
            if (selectedItems.Count > 1)
            {
                shippers = selectedItems.ToList();
               shipperService.DeleteAllShippers(shippers);
                snackBar.Add("All Companies Deleted.", Severity.Error);
            }
            selectedItems = null;
        }

        //get shipper id
        int ID;
        Shipper entity;
        private void getID()
        {
            entity = shipperService.GetShipperById(ID);
        }

    

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IDialogService DialogService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private MudBlazor.ISnackbar snackBar { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private MudBlazorCRUD_Dialog_App.Services.ICompanyService companyService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private MudBlazorCRUD_Dialog_App.Services.IShipperService shipperService { get; set; }
    }
}
#pragma warning restore 1591
